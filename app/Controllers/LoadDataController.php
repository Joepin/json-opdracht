<?php

namespace App\Controllers;

use App\Factories\DataLoaderFactory;
use App\Jobs\ProcessAccountUpload;
use Exception;
use Laravel\Lumen\Routing\Controller as BaseController;

class LoadDataController extends BaseController
{

    protected $dataLoaderFactory;

    public function __construct(DataLoaderFactory $dataLoaderFactory)
    {
        $this->dataLoaderFactory = $dataLoaderFactory;
    }

    public function handle(string $path): void {
        $file = $this->loadFromFile($path);
        $extension = pathinfo($path, PATHINFO_EXTENSION);

        $loader = $this->dataLoaderFactory->getDataLoader($extension);
        $array = $loader->loadData($file);


        $this->dispatchAccountJobs($array);

    }

    public function dispatchAccountJobs(array $accounts): void {
        foreach($accounts as $account) {
            $this->dispatch(new ProcessAccountUpload($account));
        }
    }

    public function loadFromFile(string $path): string {
        try {
            $file = file_get_contents($path);
        } catch (Exception $exception) {
            throw $exception;
        }
        return $file;
    }

}
