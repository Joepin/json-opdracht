<?php

namespace App\Interfaces;

interface DataLoaderStrategyInterface {
    public function loadData(string $file): array;
}
