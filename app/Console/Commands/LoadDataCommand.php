<?php

/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Controllers\LoadDataController;
use Illuminate\Console\Command;


/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class LoadDataCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "load:data {path}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Loads data into database";


    /**
     * Execute the console command.
     *
     * @param LoadDataController $loadDataController
     */
    public function handle(LoadDataController $loadDataController)
    {
        $path = $this->argument('path');
        $loadDataController->handle($path);
    }
}
