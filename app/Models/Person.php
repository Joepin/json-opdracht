<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Person extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hash', 'name', 'address', 'checked', 'description', 'interest', 'date_of_birth',
        'email', 'account'
    ];
    public $timestamps = false;

    protected $dates = [
        'date_of_birth',
    ];

    public function creditcard(): BelongsTo
    {
        return $this->BelongsTo(CreditCard::class);
    }
}
