<?php

namespace App\Jobs;

use App\Services\CreditCardService;
use App\Services\PersonService;
use Exception;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;


class ProcessAccountUpload extends Job implements ShouldQueue
{

    use Batchable, Queueable;
    public $account;

    /**
     * Create a new job instance.
     *
     * @param array $account
     */
    public function __construct(array $account)
    {
        $this->account = $account;
    }

    /**
     * Execute the job.
     *
     * @param PersonService $personService
     * @param CreditCardService $creditCardService
     * @return void
     * @throws Exception
     */
    public function handle(PersonService $personService, CreditCardService $creditCardService)
    {
        $save = false;
        $accountDataType = gettype($this->account['date_of_birth']);

        if ($accountDataType !== 'NULL'){
            $date = $personService->convertDate($this->account['date_of_birth']);

            if ($personService->isAgedAppropriate($date)) {
                $this->account = array_replace($this->account, ['date_of_birth' => $date]);
                $save = true;
            }
        }
        else {
            $save = true;
        }

        if($save) {
            DB::beginTransaction();
            try
            {
                $person = $personService->create($this->account);
                $creditCardService->create($this->account['credit_card'], $person);
                DB::commit();
            }
            catch (Exception $e)
            {
                DB::rollback();
                throw $e;
            }
        }
    }
}
