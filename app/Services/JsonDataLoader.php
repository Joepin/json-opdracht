<?php

namespace App\Services;

use App\Interfaces\DataLoaderStrategyInterface;

class JsonDataLoader implements DataLoaderStrategyInterface {

    /**
     * @param string $file
     * @return array
     */
    public function loadData(string $file): array
    {
        try {
           $contents = json_decode($file, true);
        } catch (\Exception $exception) {
            throw $exception;
        }
        return $contents?: [];
    }
}
