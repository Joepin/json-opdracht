<?php

namespace App\Services;

use App\Models\Person;
use Carbon\Carbon;

class PersonService
{

    public function create(array $data): Person {
        $data['hash'] = md5(serialize($data));
        return $this->save(new Person, $data);
    }

    private function save(Person $person, array $data): Person {
        $person->fill($data);
        $person->save();
        return $person;
    }

    public function isAgedAppropriate($date): bool {
        $age = Carbon::parse($date)->age;
        return $age > 18 && $age < 65;
    }

    public function convertDate($date): string
    {
        try {
            return Carbon::createFromFormat('d/m/Y', $date);
        } catch (\Exception $e) {}
        try {
            return Carbon::createFromFormat('Y-m-d H:i:s', $date);
        } catch (\Exception $e) {}

        return Carbon::parse($date);
    }


}
