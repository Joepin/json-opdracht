<?php

namespace App\Services;

use App\Models\CreditCard;
use App\Models\Person;

class CreditCardService
{

    public function create(array $data, Person $person): CreditCard{
        return $this->save(new CreditCard, $person, $data);
    }

    private function save(CreditCard $creditCard, Person $person, array $data): CreditCard{
        $creditCard->fill($data);
        $creditCard->person()->associate($person);
        $creditCard->save();
        return $creditCard;
    }
}
