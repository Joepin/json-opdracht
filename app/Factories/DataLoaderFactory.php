<?php

namespace App\Factories;

use App\Interfaces\DataLoaderStrategyInterface;
use App\Services\JsonDataLoader;

class DataLoaderFactory {

    public function getDataLoader(string $format): ?DataLoaderStrategyInterface {
        switch (strtolower($format)){
            case 'json':
                return new JsonDataLoader();

            default: throw new \Exception('File not supported');
        }
    }
}
